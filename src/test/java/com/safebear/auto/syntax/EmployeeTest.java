package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {
    @Test
    public void testEmployee(){
        // This is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        // Create a sales employee object
        SalesEmployee victoria = new SalesEmployee();
        // Create an office employee object
        OfficeEmployee laurence = new OfficeEmployee();

        // This is where we employ hannah and fire bob
        hannah.employ();
        bob.fire();

        // This is where we employ victoria and give her a bmw
        victoria.employ();
        victoria.changeCar("bmw");

        // This is where we employ laurence and assign him a desk number
        laurence.employ();
        laurence.changeDeskNumber("3456");

        // Let's print their state to screen
        System.out.println("Hannah's employment state: " + hannah.isEmployed());
        System.out.println("Bob's employment state: " + bob.isEmployed());
        System.out.println("Victoria's employment state: " + victoria.isEmployed());
        System.out.println("Victoria's car: " + victoria.car);
        System.out.println("Laurence's employment state: " + laurence.isEmployed());
        System.out.println("Laurence's desk number: " + laurence.deskNumber);

    }
}
