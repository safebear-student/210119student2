package com.safebear.auto.nonBddTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import utils.Utils;

public class BaseTest {
        WebDriver driver;
        LoginPage loginPage;
        ToolsPage toolsPage;

        @BeforeTest
        public void setUp() {
            driver = Utils.getDriver();
            loginPage = new LoginPage(driver);
            toolsPage = new ToolsPage(driver);
        }

        @AfterTest
        public void tearDown() {
            try {
                Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            driver.quit();
        }

    }