package com.safebear.auto.nonBddTests;

import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Utils;

public class LoginTests extends BaseTest {

    @Test
    public void validLogin(){
        // Action - go to login page
        driver.get(Utils.getUrl());
        // Expected - check I'm on Login page
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // Action - login as valid user
        loginPage.enterUserCredentials("tester","letmein");
        loginPage.clickLoginButton();
        // Expected - check I'm on Tools page
        Assert.assertEquals(toolsPage.getPageTitle(),toolsPage.getExpectedPageTitle());
        // Expected - check login success message is displayed
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Success"));
    }

    @Test
    public void invalidLogin(){
        // Action - go to login page
        driver.get(Utils.getUrl());
        // Expected - check I'm on Login page
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // Action - login as invalid user
        loginPage.enterUserCredentials("attacker","garbage");
        loginPage.clickLoginButton();
        // Expected - check I'm on Login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page");
        // Expected - check warning message is displayed
        Assert.assertTrue(loginPage.checkForFailedLoginwarning().contains("incorrect"));
    }
}
