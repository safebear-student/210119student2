package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.Utils;

public class StepDefs {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp(){
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown(){
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep","2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        // Open our web application in the browser
        driver.get(Utils.getUrl());
        // Check we're on the login page
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle(), "We're not on the login page or its title has changed");
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_USER(String user) throws Throwable {
        switch (user){
            case "invalidUser":
                loginPage.enterUserCredentials("attacker","garbage1");
                loginPage.clickLoginButton();
                break;
            case "validUser":
                loginPage.enterUserCredentials("tester","letmein");
                loginPage.clickLoginButton();
                break;
                default:
                    Assert.fail("The test data is wrong - the only values that can be accepted are 'validUser' or 'invalidUser'");
                    break;
        }
    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_following_MESSAGE(String validationMessage) throws Throwable {
        switch(validationMessage){
            case "Username or Password is incorrect":
                Utils.captureScreenshot(driver,Utils.generateScreenShotFileName("invalidLogin"));
                Assert.assertTrue(loginPage.checkForFailedLoginwarning().contains(validationMessage));
                break;
            case "Login Successful":
                Utils.captureScreenshot(driver,Utils.generateScreenShotFileName("validLogin"));
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(validationMessage));
                break;
                default:
                    Assert.fail("The test data is wrong");
                    break;
        }
    }
}
