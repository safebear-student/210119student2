package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {
    // messages
    private By successfulLoginMessage = By.xpath(".//body/div[@class = 'container']/p/b");

    // tool actions - remove
    private By removeFirstToolInList = By.xpath("//*[@id='remove']");
    private By removeSecondToolInList = By.xpath("//*[@id='remove']");
}
