package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {
    ToolsPageLocators locators = new ToolsPageLocators();
    @NonNull
    WebDriver driver;
    String expectedPageTitle = "Tools Page";

    public String getPageTitle(){
        return driver.getTitle();
    }

    public String getExpectedPageTitle(){
        return expectedPageTitle;
    }

    public String checkForLoginSuccessfulMessage(){
        return driver.findElement(locators.getSuccessfulLoginMessage()).getText();
    }
}
