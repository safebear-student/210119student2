package utils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {
    // run locally
    private static final String URL = System.getProperty("url","http://localhost:8080");

    // run actual website
    // private static final String URL = System.getProperty("url","http://toolslist.safebear.co.uk:8080");

    private static final String BROWSER = System.getProperty("browser","chrome");

    public static String getUrl(){
        return URL;
    }

    public static String generateScreenShotFileName(String testname){
        // create filename
        return testname + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".png";
    }

    public static void captureScreenshot(WebDriver driver, String fileName){
        // take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        // ensure the 'screenshots' directory exists
        File file = new File("target/screenshots/");
        if (!file.exists()){
            if (file.mkdir()){
                System.out.println("Directory is created");
            }
            else {
                System.out.println("Failed to create directory");
            }
        }

        // copy file to filename and location previously set
        try {
            copy(scrFile, new File("target/screenshots/" + fileName));
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.firefox.driver","src/test/resources/drivers/geckodriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

//            case "firefox":
//                return new FirefoxDriver(options);
//
            case "headless":
                options.addArguments("headless","disable-gpu");
                return new ChromeDriver(options);

            default:
                return new ChromeDriver(options);
        }
    }
}
